package com.fesquivel.kittieapp.detail.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class DetailMapperTest {

    private static final String EMPTY = "";

    private DetailMapper mapper;

    @Before
    public void setup() {
        mapper = new DetailMapper();
    }

    @Test
    public void whenResponseEmptyListThenReturnNameEmpty() throws DetailParserException {
        Assert.assertEquals(EMPTY, mapper.mapTo(Collections.emptyList()).getName());
    }

    @Test
    public void whenResponseEmptyListThenReturnImageUriEmpty() throws DetailParserException {
        Assert.assertEquals(EMPTY, mapper.mapTo(Collections.emptyList()).getImageUri());
    }

    @Test
    public void whenResponseEmptyListThenReturnItemsEmpty() throws DetailParserException {
        Assert.assertEquals(Collections.emptyList(), mapper.mapTo(Collections.emptyList()).getItems());
    }
}
