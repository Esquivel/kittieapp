package com.fesquivel.kittieapp.detail.ui;


import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.detail.repository.DetailRepository;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DetailPresenterTest {

    private static final String STRING = "STRING";

    private DetailView<DetailPresenter> view;
    private DetailState state;
    private DetailPresenter presenter;

    @Before
    public void setup() {
        final DetailRepository<Detail> repository =
                (DetailRepository<Detail>) mock(DetailRepository.class);
        view = (DetailView<DetailPresenter>) mock(DetailView.class);
        state = mock(DetailState.class);

        presenter = new DetailPresenter(repository);
        presenter.setState(state);
        presenter.view = view;
    }

    @Test
    public void whenOnRefreshThenShowLoadingTrue() {
        when(state.getBreedId()).thenReturn(STRING);

        presenter.onRefresh();

        verify(view).showLoading(true);
    }

    @Test
    public void whenExternalButtonClickedThenViewStartDeepLinkScreen() {
        presenter.onExternalButtonClicked(STRING);

        verify(view).startDeeplinkScreen(STRING);
    }
}
