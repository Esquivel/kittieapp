package com.fesquivel.kittieapp.commons;

import com.fesquivel.kittieapp.home.repository.BreedApi;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiProvider {

    private static final String BASE_URI = "https://api.thecatapi.com/v1/";
    private static final String HEADER_EXTERNAL = "x-api-key";
    private static final String API_KEY_EXTERNAL = "DEMO-API-KEY";

    private static ApiProvider instance;

    private ApiProvider() {
        //Nothing to do.
    }

    /**
     * Static factory method to get a instance of {@link ApiProvider}.
     *
     * @return The instance.
     */
    public static ApiProvider provide() {
        if (instance == null) {
            instance = new ApiProvider();
        }

        return instance;
    }

    public BreedApi provideBreedApi() {
        return getRetrofitConfig().create(BreedApi.class);
    }

    private Retrofit getRetrofitConfig() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.interceptors().add(chain -> {
            final Request newRequest = chain.request().newBuilder()
                    .addHeader(HEADER_EXTERNAL, API_KEY_EXTERNAL).build();
            return chain.proceed(newRequest);
        });

        okHttpClientBuilder.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl(BASE_URI)
                .client(okHttpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
