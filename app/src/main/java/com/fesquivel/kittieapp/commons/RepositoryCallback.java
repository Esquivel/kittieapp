package com.fesquivel.kittieapp.commons;

public interface RepositoryCallback<T> {

    void onSuccess(T t);

    void onFailed(Throwable throwable);
}
