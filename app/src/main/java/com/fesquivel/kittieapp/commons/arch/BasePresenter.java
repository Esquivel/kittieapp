package com.fesquivel.kittieapp.commons.arch;

import androidx.annotation.NonNull;

public abstract class BasePresenter<T extends BaseState> implements Mvp.StatePresenter<T> {

    private T state;

    @NonNull
    @Override
    public T getState() {
        return state;
    }

    @Override
    public void setState(@NonNull final T state) {
        this.state = state;
    }
}