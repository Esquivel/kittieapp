package com.fesquivel.kittieapp.commons.arch

import java.io.Serializable

interface Mvp {

    /**
     * View contract.
     *
     * @param <T> The [Presenter] type.
    </T> */
    interface View<T> where T: Presenter {

        /**
         * Set the presenter.
         *
         * @param presenter The presenter.
         */
        fun setPresenter(presenter: T)

        /**
         * Get the presenter.
         */
        fun getPresenter(): T
    }

    interface Presenter {

        /**
         * Start presenter.
         */
        fun start()

        /**
         * Stop presenter.
         */
        fun stop()
    }

    interface State : Serializable

    interface StatePresenter<S : State> : Presenter {

        /**
         * The presenter state.
         */
        var state: S
    }
}