package com.fesquivel.kittieapp.home.repository;

import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.RepositoryCallback;

public interface HomeRepository<T> {

    /**
     * Get the information.
     */
    void get();

    /**
     * Sets the callback.
     *
     * @param callback The callback instance or null.
     */
    void setCallback(@Nullable final RepositoryCallback<T> callback);
}
