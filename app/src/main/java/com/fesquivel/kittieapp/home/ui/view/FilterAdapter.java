package com.fesquivel.kittieapp.home.ui.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.ui.FilterAdapterCallback;

import java.util.ArrayList;
import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterViewHolder> {

    @Nullable
    private FilterAdapterCallback callback;
    private final List<Filter> itemList = new ArrayList<>();

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new FilterAdapter.FilterViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.filter_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterViewHolder holder, final int position) {
        final Filter filter = itemList.get(position);

        holder.nameFilterView.setText(filter.getDescription());
        holder.containerView.setOnClickListener(v -> {
            if (callback != null) {
                callback.onFilterClicked(filter);
            }
        });
        holder.containerView.setSelected(filter.isChecked());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setItemList(final List<Filter> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);

        notifyDataSetChanged();
    }

    public void setCallback(@Nullable final FilterAdapterCallback callback) {
        this.callback = callback;
    }

    static class FilterViewHolder extends RecyclerView.ViewHolder {

        final View containerView;
        final TextView nameFilterView;

        FilterViewHolder(@NonNull final View itemView) {
            super(itemView);

            containerView = itemView.findViewById(R.id.filterContainer);
            nameFilterView = itemView.findViewById(R.id.nameFilter);
        }
    }
}
