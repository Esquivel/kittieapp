package com.fesquivel.kittieapp.home.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.arch.BaseState;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.domain.Home;

public class HomeState extends BaseState {

    private static final long serialVersionUID = 7588145465534906607L;

    @Nullable
    private Home<Filter, Cat> home;

    @Nullable
    public Home<Filter, Cat> getHome() {
        return home;
    }

    public void setHome(@NonNull final Home<Filter, Cat> home) {
        this.home = home;
    }
}
