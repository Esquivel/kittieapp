package com.fesquivel.kittieapp.home.repository;

import com.fesquivel.kittieapp.commons.ApiProvider;
import com.fesquivel.kittieapp.commons.SchedulerProvider;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.domain.Home;

public class CatContainer {

    private HomeRepository<Home<Filter, Cat>> repository;
    private BreedApi breedApi;
    private SchedulerProvider schedulerProvider;
    private ItemMapper itemMapper;

    public HomeRepository<Home<Filter, Cat>> provideCatRepository() {
        if (repository == null) {
            repository = new DataHomeRepository(provideBreedApi(), provideSchedulerProvider(),
                    provideItemMapper());
        }
        return repository;
    }

    private ItemMapper provideItemMapper() {
        if (itemMapper == null) {
            itemMapper = new ItemMapper();
        }
        return itemMapper;
    }

    private SchedulerProvider provideSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProvider();
        }
        return schedulerProvider;
    }

    private BreedApi provideBreedApi() {
        if (breedApi == null) {
            breedApi = ApiProvider.provide().provideBreedApi();
        }
        return breedApi;
    }
}
