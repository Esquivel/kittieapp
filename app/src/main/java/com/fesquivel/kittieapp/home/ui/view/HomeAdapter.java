package com.fesquivel.kittieapp.home.ui.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.ui.DataAdapterCallback;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Nullable
    private DataAdapterCallback callback;
    private final List<Cat> itemList = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                                      final int viewType) {
        return new CatViewHolder(inflate(parent, R.layout.cat_item_layout));
    }

    private View inflate(final ViewGroup parent, @LayoutRes final int layoutResId) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final Cat cat = itemList.get(position);
        createCatViewHolder(holder, cat);
    }

    @Override
    public int getItemViewType(final int position) {
        return itemList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setData(final List<Cat> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);

        notifyDataSetChanged();
    }

    public void setCallback(@Nullable final DataAdapterCallback callback) {
        this.callback = callback;
    }

    private void createCatViewHolder(final RecyclerView.ViewHolder holder, final Cat cat) {
        final CatViewHolder viewHolder = (CatViewHolder) holder;

        viewHolder.containerView.setOnClickListener(v -> {
            if (callback != null) {
                callback.onCatClicked(cat);
            }
        });
        viewHolder.descriptionView.setText(cat.getDescription());
        viewHolder.titleView.setText(cat.getBreed());

//        Picasso.with(viewHolder.getImageView().getContext())
//                .load(cat.getImageUri())
//                //TODO add placeholder
//                .fit()
//                .centerCrop()
//                .into(viewHolder.getImageView());
    }

    static class CatViewHolder extends RecyclerView.ViewHolder {

        final View containerView;
        final ImageView imageView;
        final TextView titleView;
        final TextView descriptionView;

        public CatViewHolder(@NonNull final View itemView) {
            super(itemView);

            containerView = itemView.findViewById(R.id.container);
            imageView = itemView.findViewById(R.id.image);
            titleView = itemView.findViewById(R.id.title);
            descriptionView = itemView.findViewById(R.id.description);
        }
    }
}