package com.fesquivel.kittieapp.home.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.domain.Item;

public interface Cat extends Item {

    /**
     * Get id.
     *
     * @return The id.
     */
    @NonNull
    String getId();

    /**
     * Get the breed name.
     *
     * @return The breed name.
     */
    @NonNull
    String getBreed();

    /**
     * Get the description.
     *
     * @return The description.
     */
    @NonNull
    String getDescription();

    /**
     * The origin country.
     *
     * @return The country.
     */
    @NonNull
    String getCountryCode();

    /**
     * Returns the country name.
     *
     * @return The country name.
     */
    @NonNull
    String getCountryName();

    /**
     * Get the image.
     *
     * @return The image uri.
     */
    @NonNull
    String getImageUri();

    /**
     * Get the wikipedia uri.
     *
     * @return The uri.
     */
    @Nullable
    String getWikiUri();
}