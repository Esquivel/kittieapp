package com.fesquivel.kittieapp.home.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fesquivel.kittieapp.R
import com.fesquivel.kittieapp.commons.arch.BaseActivity
import com.fesquivel.kittieapp.detail.ui.DetailActivity
import com.fesquivel.kittieapp.home.domain.Cat
import com.fesquivel.kittieapp.home.domain.Filter
import com.fesquivel.kittieapp.home.repository.CatContainer
import com.fesquivel.kittieapp.home.ui.view.FilterAdapter
import com.fesquivel.kittieapp.home.ui.view.HomeAdapter
import com.google.android.material.snackbar.Snackbar

private const val BREED_ID = "breed_id"

class HomeActivity : BaseActivity<HomeState, HomePresenter>(), HomeView<HomePresenter> {

    private lateinit var filterAdapter: FilterAdapter
    private lateinit var dataAdapter: HomeAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
        setToolbar()

        dataAdapter = HomeAdapter()
        filterAdapter = FilterAdapter()

        val filterRecyclerView = findViewById<RecyclerView>(R.id.filterRecycler)
        filterRecyclerView.layoutManager = LinearLayoutManager(this,
            LinearLayoutManager.HORIZONTAL, false)
        filterRecyclerView.adapter = filterAdapter

        val dataRecyclerView = findViewById<RecyclerView>(R.id.dataRecycler)
        dataRecyclerView.layoutManager = LinearLayoutManager(this)
        dataRecyclerView.adapter = dataAdapter

        swipeRefreshLayout = findViewById(R.id.swipeRefresh)
        swipeRefreshLayout.setOnRefreshListener {
            getPresenter().onRefresh()
        }
    }

    override fun onCreatePresenter(): HomePresenter {
        return HomePresenter(CatContainer().provideCatRepository())
    }

    override fun onCreateState(): HomeState {
        return HomeState()
    }

    override fun onStart() {
        super.onStart()
        getPresenter().view = this
    }

    override fun onResume() {
        super.onResume()

        filterAdapter.setCallback {
            getPresenter().onFilterClicked(it)
        }

        dataAdapter.setCallback {
            getPresenter().onCardClicked(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val view = super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.detail_menu, menu)
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh) {
            getPresenter().onRefresh()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        super.onStop()
        getPresenter().view = null
    }

    private fun setToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.home_title_activity)
    }

    override fun showListData(filterList: MutableList<Filter>, dataList: MutableList<Cat>) {
        filterAdapter.setItemList(filterList)
        dataAdapter.setData(dataList)
    }

    override fun startDetailScreen(cat: Cat) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(BREED_ID, cat.id)

        startActivity(intent)
    }

    override fun showLoading(enabled: Boolean) {
        swipeRefreshLayout.isRefreshing = enabled
    }

    override fun showErrorMessage() {
        Snackbar.make(swipeRefreshLayout, getString(R.string.retry_error_message),
            Snackbar.LENGTH_SHORT).show()
    }
}