package com.fesquivel.kittieapp.home.repository;

import com.fesquivel.kittieapp.domain.ImageResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BreedApi {

    /**
     * Get data.
     *
     * @return The data.
     */
    @GET("breeds")
    Observable<List<BreedsResponse>> getBreads();

    /**
     * Get image data.
     *
     * @param breedId The breed id.
     * @return The data.
     */
    @GET("images/search")
    Observable<List<ImageResponse>> getImage(@Query("breed_id") final String breedId);
}