package com.fesquivel.kittieapp.home.domain;

import androidx.annotation.NonNull;

import java.util.Objects;

public final class CountryFilter implements Filter {

    private static final long serialVersionUID = -6575317765841486156L;

    private final String key;
    private final String description;
    private final boolean checked;

    CountryFilter(final Builder builder) {
        key = builder.key;
        description = builder.description;
        checked = builder.checked;
    }

    @NonNull
    @Override
    public String getKey() {
        return key;
    }

    @NonNull
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public int getType() {
        return HomeType.FILTER;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !getClass().equals(o.getClass())) {
            return false;
        }
        final CountryFilter that = (CountryFilter) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public int compareTo(final Filter o) {
        return description.compareTo(o.getDescription());
    }

    public static final class Builder {

        final String key;
        final String description;
        boolean checked;

        public Builder(@NonNull final String key, @NonNull final String description) {
            this.key = key;
            this.description = description;
        }

        public Builder(@NonNull final Filter filter) {
            this(filter.getKey(), filter.getDescription());
        }

        public Builder withChecked(final boolean checked) {
            this.checked = checked;
            return this;
        }

        @NonNull
        public Filter build() {
            return new CountryFilter(this);
        }
    }
}