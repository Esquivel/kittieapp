package com.fesquivel.kittieapp.home.domain;

import androidx.annotation.NonNull;

import com.fesquivel.kittieapp.domain.Item;

public interface Filter extends Item, Comparable<Filter> {

    /**
     * Returns the key to apply a filter.
     *
     * @return The key.
     */
    @NonNull
    String getKey();

    /**
     * Returns the description to show.
     *
     * @return The description.
     */
    @NonNull
    String getDescription();

    /**
     * Returns if is checked.
     *
     * @return True if is checked.
     */
    boolean isChecked();
}
