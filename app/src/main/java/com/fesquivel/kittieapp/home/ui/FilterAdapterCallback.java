package com.fesquivel.kittieapp.home.ui;

import com.fesquivel.kittieapp.home.domain.Filter;

public interface FilterAdapterCallback {

    /**
     * Called when user clicks a filter.
     *
     * @param filter The {@link Filter} instance.
     */
    void onFilterClicked(final Filter filter);
}
