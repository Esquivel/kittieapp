package com.fesquivel.kittieapp.home.repository;

import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.RepositoryCallback;
import com.fesquivel.kittieapp.commons.SchedulerProvider;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.domain.Home;

import java.util.Collections;

import io.reactivex.disposables.Disposable;

class DataHomeRepository implements HomeRepository<Home<Filter, Cat>> {

    private final BreedApi api;
    private final SchedulerProvider schedulerProvider;
    private final ItemMapper mapper;

    @Nullable
    private RepositoryCallback<Home<Filter, Cat>> callback;
    private Disposable disposable;

    DataHomeRepository(final BreedApi api, final SchedulerProvider schedulerProvider,
                       final ItemMapper mapper) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
        this.mapper = mapper;
    }

    @Override
    public void get() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        disposable = api.getBreads()
                .map(list-> {
                        Collections.sort(list);
                        return mapper.mapTo(list);
                })
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(this::onSuccess, this::onFailed);
    }

    @Override
    public void setCallback(@Nullable final RepositoryCallback<Home<Filter, Cat>> callback) {
        this.callback = callback;
    }

    private void onSuccess(final Home<Filter, Cat> home) {
        if (callback != null) {
            callback.onSuccess(home);
        }
    }

    private void onFailed(final Throwable throwable) {
        if (callback != null) {
            callback.onFailed(throwable);
        }
    }
}