package com.fesquivel.kittieapp.home.domain;

import com.fesquivel.kittieapp.domain.Item;

import java.io.Serializable;
import java.util.List;

public interface Home<T extends Item, S extends Item> extends Serializable {

    /**
     * Returns the filter list.
     *
     * @return The filter list.
     */
    List<T> getFilterList();

    /**
     * Returns the data list.
     *
     * @return The data list.
     */
    List<S> getDataList();
}
