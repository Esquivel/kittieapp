package com.fesquivel.kittieapp.home.ui;

import androidx.annotation.NonNull;

import com.fesquivel.kittieapp.commons.arch.Mvp;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.Filter;

import java.util.List;

public interface HomeView<T extends Mvp.Presenter> extends Mvp.View<T> {

    /**
     * Show list data.
     *
     * @param filterList The filter list.
     * @param dataList The data list.
     */
    void showListData(@NonNull final List<Filter> filterList, @NonNull final List<Cat> dataList);

    /**
     * Start the detail screen.
     *
     * @param cat The cat information.
     */
    void startDetailScreen(final Cat cat);

    /**
     * Show loading.
     *
     * @param enabled If its enabled.
     */
    void showLoading(final boolean enabled);

    /**
     * Show a error messagge.
     */
    void showErrorMessage();
}
