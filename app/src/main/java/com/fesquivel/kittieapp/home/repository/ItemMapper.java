package com.fesquivel.kittieapp.home.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.fesquivel.kittieapp.home.domain.HomeType;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.CountryFilter;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.domain.Home;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This is the last point where we use the model defined by backend: {@link BreedsResponse}, a DTO.
 * From this point, use a internal representation, {@link Home} in this case. This is approach
 * offers a decoupled communication to backend services.
 *
 * If at any moment backend change their DTO, we have this layer to ignore that new contract, and
 * assurance the proper app functionality.
 */
final class ItemMapper {

    /**
     * This method maps to a {@link Home}.
     * Here could apply validations over {@link BreedsResponse} to accurate a valid model
     * representation.
     *
     * It is really important to execute this validations on a WorkerThread.
     *
     * @param responseList The response to map.
     * @return A {@link Home} instance.
     */
    @WorkerThread
    @NonNull
    Home<Filter, Cat> mapTo(final List<BreedsResponse> responseList) {
        final List<Cat> cats = new ArrayList<>();
        final Set<Filter> filterSet = new HashSet<>();

        if (responseList.isEmpty()) {
            return new InternalHome(new ArrayList<>(), new ArrayList<>());
        }

        for (final BreedsResponse each: responseList) {
            if (isValid(each)) {
                filterSet.add(new CountryFilter.Builder(each.countryCode, each.countryName).build());
                cats.add(new InternalCat(each));
            }
        }

        final List<Filter> filterList = new ArrayList<>(filterSet);
        Collections.sort(filterList);

        return new InternalHome(filterList, cats);
    }

    private boolean isValid(final BreedsResponse response) {
        return response.id != null && !response.id.isEmpty() && response.name != null
                && !response.name.isEmpty() && response.countryCode != null
                && !response.countryCode.isEmpty();
    }

    static final class InternalHome implements Home<Filter, Cat> {

        private static final long serialVersionUID = -8331715841032311082L;

        private final List<Filter> filterList;
        private final List<Cat> dataList;

        InternalHome(final List<Filter> filterList, final List<Cat> dataList) {
            this.filterList = filterList;
            this.dataList = dataList;
        }

        @Override
        public List<Filter> getFilterList() {
            return filterList;
        }

        @Override
        public List<Cat> getDataList() {
            return dataList;
        }
    }

    static final class InternalCat implements Cat {

        private static final long serialVersionUID = 3262275889702593444L;

        private final String id;
        private final String breed;
        private final String description;
        private final String countryCode;
        private final String imageUri;
        private final String wikiUri;
        private final String countryName;

        InternalCat(final BreedsResponse response) {
            id = response.id;
            description = response.description;
            breed = response.name;
            countryCode = response.countryCode;
            imageUri = "";
            wikiUri = response.wikiUri;
            countryName = response.countryName;
        }

        @NonNull
        @Override
        public String getId() {
            return id;
        }

        @NonNull
        @Override
        public String getBreed() {
            return breed;
        }

        @NonNull
        @Override
        public String getDescription() {
            return description;
        }

        @NonNull
        @Override
        public String getCountryCode() {
            return countryCode;
        }

        @NonNull
        @Override
        public String getCountryName() {
            return countryName;
        }

        @NonNull
        @Override
        public String getImageUri() {
            return imageUri;
        }

        @Nullable
        @Override
        public String getWikiUri() {
            return wikiUri;
        }

        @Override
        public int getType() {
            return HomeType.CONTENT;
        }
    }
}