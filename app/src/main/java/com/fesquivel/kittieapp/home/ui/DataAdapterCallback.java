package com.fesquivel.kittieapp.home.ui;

import com.fesquivel.kittieapp.home.domain.Cat;

public interface DataAdapterCallback {

    /**
     * Called whe the user clicks a Cat.
     *
     * @param cat The {@link Cat} instance.
     */
    void onCatClicked(final Cat cat);
}
