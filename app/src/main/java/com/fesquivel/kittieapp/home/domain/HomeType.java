package com.fesquivel.kittieapp.home.domain;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        HomeType.CONTENT,
        HomeType.FILTER
})
public @interface HomeType {
    int CONTENT = 0;
    int FILTER = 1;
}