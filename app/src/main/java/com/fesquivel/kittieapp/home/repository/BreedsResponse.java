package com.fesquivel.kittieapp.home.repository;

import com.google.gson.annotations.SerializedName;

public final class BreedsResponse implements Comparable<BreedsResponse> {

    public final String id;
    public final String name;
    public final String description;
    @SerializedName("wikipedia_url")
    public final String wikiUri;
    @SerializedName("country_code")
    public final String countryCode;
    @SerializedName("origin")
    public final String countryName;
    public final String temperament;

    public BreedsResponse(final String id, final String name, final String description,
                          final String wikiUri, final String countryCode, final String countryName,
                          final String temperament) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.wikiUri = wikiUri;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.temperament = temperament;
    }

    @Override
    public int compareTo(final BreedsResponse o) {
        if (name == null || o.name == null) {
            return 0;
        }
        return name.compareTo(o.name);
    }
}