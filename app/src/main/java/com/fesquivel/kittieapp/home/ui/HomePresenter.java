package com.fesquivel.kittieapp.home.ui;

import androidx.annotation.NonNull;

import com.fesquivel.kittieapp.commons.arch.BasePresenter;
import com.fesquivel.kittieapp.home.domain.Cat;
import com.fesquivel.kittieapp.home.domain.CountryFilter;
import com.fesquivel.kittieapp.home.domain.Filter;
import com.fesquivel.kittieapp.home.domain.Home;
import com.fesquivel.kittieapp.home.repository.HomeRepository;
import com.fesquivel.kittieapp.commons.RepositoryCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomePresenter extends BasePresenter<HomeState> {

    private final HomeRepository<Home<Filter, Cat>> homeRepository;
    HomeView<HomePresenter> view;

    public HomePresenter(final HomeRepository<Home<Filter, Cat>> homeRepository) {
        this.homeRepository = homeRepository;
    }

    @Override
    public void start() {
        homeRepository.setCallback(new RepositoryCallback<Home<Filter, Cat>>() {
            @Override
            public void onSuccess(final Home<Filter, Cat> home) {
                getState().setHome(home);
                final Home<Filter, Cat> innerHome = getState().getHome();
                showData(innerHome.getFilterList(), innerHome.getDataList());
            }

            @Override
            public void onFailed(final Throwable throwable) {
                view.showLoading(false);
                view.showErrorMessage();
            }
        });

        if (getState().getHome() != null) {
            final Home<Filter, Cat> innerHome = getState().getHome();
            showData(innerHome.getFilterList(), innerHome.getDataList());
        } else {
            getData();
        }
    }

    @Override
    public void stop() {
        homeRepository.setCallback(null);
    }

    /**
     * Called when occurs a refresh operation in View.
     */
    public void onRefresh() {
        getData();
    }

    /**
     * Called when the user click on a Card.
     *
     * @param cat The cat clicked.
     */
    public void onCardClicked(final Cat cat) {
        view.startDetailScreen(cat);
    }

    /**
     * Called when the user click on a Filter.
     *
     * @param filter The Filter clicked.
     */
    public void onFilterClicked(final Filter filter) {
        final List<Filter> updatedFilters = getUpdatedFilterList(filter);

        showData(updatedFilters, getState().getHome().getDataList());
    }

    private void getData() {
        view.showLoading(true);
        homeRepository.get();
    }

    private List<Filter> getUpdatedFilterList(final Filter filter) {
        final List<Filter> filters = getState().getHome().getFilterList();
        final int filterIndex = filters.indexOf(filter);
        final Filter filterToChange = filters.get(filterIndex);
        final Filter filterApplied = new CountryFilter.Builder(filter)
                .withChecked(!filterToChange.isChecked())
                .build();

        filters.set(filterIndex, filterApplied);
        return filters;
    }

    private List<Cat> getFilteredCatList(final List<Filter> filterList, final List<Cat> catList) {
        final List<Filter> checkedFilters = getCheckedFilterList(filterList);

        final List<Cat> resultCatList = new ArrayList<>();
        for (final Cat cat : catList) {
            for (final Filter filter : checkedFilters) {
                if (filter.getKey().equals(cat.getCountryCode())) {
                    resultCatList.add(cat);
                }
            }
        }

        if (resultCatList.isEmpty()) {
            return Collections.unmodifiableList(catList);
        }

        return Collections.unmodifiableList(resultCatList);
    }

    @NonNull
    private List<Filter> getCheckedFilterList(final List<Filter> filters) {
        final List<Filter> checkedFilters = new ArrayList<>();
        for (final Filter filter : filters) {
            if (filter.isChecked()) {
                checkedFilters.add(filter);
            }
        }
        return checkedFilters;
    }

    private void showData(final List<Filter> filterList, final List<Cat> dataList) {
        view.showLoading(false);
        final List<Cat> filteredCatList = getFilteredCatList(filterList, dataList);

        view.showListData(filterList, filteredCatList);
    }
}