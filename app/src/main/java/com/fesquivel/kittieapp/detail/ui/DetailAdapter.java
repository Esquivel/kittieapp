package com.fesquivel.kittieapp.detail.ui;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.fesquivel.kittieapp.R;
import com.fesquivel.kittieapp.detail.domain.CountryItem;
import com.fesquivel.kittieapp.detail.domain.DescriptionItem;
import com.fesquivel.kittieapp.detail.domain.DetailType;
import com.fesquivel.kittieapp.detail.domain.LinkItem;
import com.fesquivel.kittieapp.detail.domain.TextItem;
import com.fesquivel.kittieapp.detail.ui.viewholder.CountryViewHolder;
import com.fesquivel.kittieapp.detail.ui.viewholder.DescriptionViewHolder;
import com.fesquivel.kittieapp.detail.ui.viewholder.LinkViewHolder;
import com.fesquivel.kittieapp.detail.ui.viewholder.TemperamentViewHolder;
import com.fesquivel.kittieapp.domain.Item;

import java.util.ArrayList;
import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Nullable
    private AdapterCallback callback;
    private final List<Item> items = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                                      final int viewType) {
        switch (viewType) {
            case DetailType.DESCRIPTION:
                return new DescriptionViewHolder(inflate(parent, R.layout.description_item_layout));
            case DetailType.COUNTRY:
                return new CountryViewHolder(inflate(parent, R.layout.country_item_layout));
            case DetailType.LINK:
                return new LinkViewHolder(inflate(parent, R.layout.link_item_layout));
            default:
                return new TemperamentViewHolder(inflate(parent, R.layout.temperament_item_layout));
        }
    }

    private View inflate(final ViewGroup parent, @LayoutRes final int layoutResId) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final Item item = items.get(position);

        switch (holder.getItemViewType()) {
            case DetailType.DESCRIPTION:
                createDescription(holder, item);
                break;
            case DetailType.COUNTRY:
                createCountry(holder, item);
                break;
            case DetailType.LINK:
                createLink(holder, item);
                break;
            default:
                createTemperament(holder, item);
                break;
        }
    }

    @Override
    public int getItemViewType(final int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(final List<Item> items) {
        this.items.clear();
        this.items.addAll(items);

        notifyDataSetChanged();
    }

    public void setCallback(@Nullable AdapterCallback callback) {
        this.callback = callback;
    }

    private void createDescription(final RecyclerView.ViewHolder holder, final Item item) {
        ((DescriptionViewHolder)holder).getDescriptionTextView().setText(((TextItem)item).getText());
    }

    private void createCountry(final RecyclerView.ViewHolder holder, final Item item) {
        final Resources resources = getResources(holder);
        final CountryItem countryItem = (CountryItem) item;
        final String text = resources.getString(R.string.country_label, countryItem.getText(),
                countryItem.getCode());

        ((CountryViewHolder)holder).getCountryTextView().setText(text);
    }

    private void createLink(final RecyclerView.ViewHolder holder, final Item item) {
        final LinkViewHolder viewHolder = (LinkViewHolder)holder;
        final LinkItem linkItem = (LinkItem) item;
        viewHolder.getLinkButtonView().setText(linkItem.getText());
        viewHolder.getLinkButtonView().setOnClickListener(v -> {
            if (callback != null) {
                callback.onExternalButtonClicked(linkItem.getLink());
            }
        });
    }

    private void createTemperament(final RecyclerView.ViewHolder holder, final Item item) {
        ((TemperamentViewHolder)holder).getTemperamentTextView().setText(((TextItem)item).getText());
    }

    @NonNull
    private Resources getResources(final RecyclerView.ViewHolder holder) {
        return holder.itemView.getContext().getResources();
    }
}
