package com.fesquivel.kittieapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;

public class CountryViewHolder extends RecyclerView.ViewHolder {

    private final TextView countryTextView;

    public CountryViewHolder(@NonNull final View itemView) {
        super(itemView);

        countryTextView = itemView.findViewById(R.id.countryText);
    }

    public TextView getCountryTextView() {
        return countryTextView;
    }
}