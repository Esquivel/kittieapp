package com.fesquivel.kittieapp.detail.ui;

import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.RepositoryCallback;
import com.fesquivel.kittieapp.commons.arch.BasePresenter;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.detail.repository.DetailRepository;

import java.util.List;

public class DetailPresenter extends BasePresenter<DetailState> {

    private final DetailRepository<Detail> repository;

    public DetailPresenter(final DetailRepository<Detail> repository) {
        this.repository = repository;
    }

    @Nullable
    DetailView<DetailPresenter> view;

    @Override
    public void start() {
        repository.setCallback(new RepositoryCallback<Detail>() {
            @Override
            public void onSuccess(final Detail detail) {
                getState().setDetail(detail);
                showData(getState().getDetail());
            }

            @Override
            public void onFailed(final Throwable throwable) {
                view.showLoading(false);
                view.showErrorMessage();
            }
        });

        final Detail detail = getState().getDetail();
        if (detail == null) {
            getData();
        } else {
            showData(detail);
        }
    }

    @Override
    public void stop() {
        repository.setCallback(null);
    }

    /**
     * Called when occurs a refresh action.
     */
    public void onRefresh() {
        getData();
    }

    /**
     * Called when user click button to an external link.
     *
     * @param uri The uri.
     */
    public void onExternalButtonClicked(final String uri) {
        view.startDeeplinkScreen(uri);
    }

    private void getData() {
        view.showLoading(true);
        repository.get(getState().getBreedId());
    }

    private void showData(final Detail detail) {
        view.showLoading(false);
        view.showTitle(detail.getName());
        view.showImage(detail.getImageUri());
        view.showItemData(detail.getItems());
    }
}
