package com.fesquivel.kittieapp.detail.repository;

import com.fesquivel.kittieapp.commons.ApiProvider;
import com.fesquivel.kittieapp.commons.SchedulerProvider;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.home.repository.BreedApi;

public class DetailContainer {

    private DetailRepository<Detail> repository;
    private SchedulerProvider schedulerProvider;
    private BreedApi api;
    private DetailMapper mapper;

    public DetailRepository<Detail> provideRepository() {
        if (repository == null) {
            repository = new BreedRepository(provideApi(), provideSchedulerProvider(),
                    provideDetailMapper());
        }
        return repository;
    }

    private BreedApi provideApi() {
        if (api == null) {
            api = ApiProvider.provide().provideBreedApi();
        }
        return api;
    }

    private SchedulerProvider provideSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProvider();
        }
        return schedulerProvider;
    }

    private DetailMapper provideDetailMapper() {
        if (mapper == null) {
            mapper = new DetailMapper();
        }
        return mapper;
    }
}
