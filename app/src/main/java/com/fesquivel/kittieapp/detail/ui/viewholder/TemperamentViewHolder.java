package com.fesquivel.kittieapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;

public class TemperamentViewHolder extends RecyclerView.ViewHolder {

    private final TextView temperamentTextView;

    public TemperamentViewHolder(@NonNull final View itemView) {
        super(itemView);

        temperamentTextView = itemView.findViewById(R.id.temperamentText);
    }

    public TextView getTemperamentTextView() {
        return temperamentTextView;
    }
}