package com.fesquivel.kittieapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;

public class LinkViewHolder extends RecyclerView.ViewHolder {

    private final TextView linkButtonView;

    public LinkViewHolder(@NonNull final View itemView) {
        super(itemView);

        linkButtonView = itemView.findViewById(R.id.linkButton);
    }

    public TextView getLinkButtonView() {
        return linkButtonView;
    }
}