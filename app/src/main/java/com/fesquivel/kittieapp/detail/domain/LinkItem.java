package com.fesquivel.kittieapp.detail.domain;

public final class LinkItem extends TextItem {

    private static final long serialVersionUID = 5677336669169372828L;
    private static final String WIKIPEDIA = "wikipedia";

    private final String link;

    public LinkItem(final String link) {
        super(WIKIPEDIA);
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    @Override
    public int getType() {
        return DetailType.LINK;
    }
}