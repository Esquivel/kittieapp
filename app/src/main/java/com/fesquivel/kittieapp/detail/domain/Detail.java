package com.fesquivel.kittieapp.detail.domain;

import com.fesquivel.kittieapp.domain.Item;

import java.io.Serializable;
import java.util.List;

public interface Detail extends Serializable {

    /**
     * Get the name.
     *
     * @return The name.
     */
    String getName();

    /**
     * Get the image uri.
     *
     * @return The uri.
     */
    String getImageUri();

    /**
     * Get the Items.
     *
     * @return The items.
     */
    List<Item> getItems();
}