package com.fesquivel.kittieapp.detail.domain;

import com.fesquivel.kittieapp.domain.Item;

public abstract class TextItem implements Item {

    private static final long serialVersionUID = 4708798652471422745L;

    private final String text;

    protected TextItem(final String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}