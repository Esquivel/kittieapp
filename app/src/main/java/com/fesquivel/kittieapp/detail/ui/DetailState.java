package com.fesquivel.kittieapp.detail.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.arch.BaseState;
import com.fesquivel.kittieapp.detail.domain.Detail;

public class DetailState extends BaseState {

    private static final long serialVersionUID = 3777558893241060087L;

    @Nullable
    private Detail detail;
    @NonNull
    private String breedId;


    public DetailState(@NonNull final String breedId) {
        this.breedId = breedId;
    }

    @Nullable
    public Detail getDetail() {
        return detail;
    }

    public void setDetail(@NonNull final Detail detail) {
        this.detail = detail;
    }

    @NonNull
    public String getBreedId() {
        return breedId;
    }

    public void setBreedId(@NonNull final String breedId) {
        this.breedId = breedId;
    }
}