package com.fesquivel.kittieapp.detail.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.fesquivel.kittieapp.detail.domain.CountryItem;
import com.fesquivel.kittieapp.detail.domain.DescriptionItem;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.detail.domain.LinkItem;
import com.fesquivel.kittieapp.detail.domain.TemperamentItem;
import com.fesquivel.kittieapp.domain.ImageResponse;
import com.fesquivel.kittieapp.domain.Item;
import com.fesquivel.kittieapp.home.repository.BreedsResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class DetailMapper {

    @WorkerThread
    @NonNull
    Detail mapTo(@Nullable final List<ImageResponse> list) throws DetailParserException {
        if (list == null) {
            throw new DetailParserException();
        }

        if (list.isEmpty()) {
            return new InternalDetail("", "", Collections.emptyList());
        }

        final ImageResponse imageResponse = list.get(0);

        if (!isValid(imageResponse)) {
            throw new DetailParserException();
        }

        if (imageResponse.responseList == null || imageResponse.responseList.isEmpty()) {
            throw new DetailParserException();
        }

        final BreedsResponse breedsResponse = imageResponse.responseList.get(0);

        final List<Item> items = new ArrayList<>();

        createCountryItemIfNeeded(breedsResponse, items);
        createTemperamentItemIfNeeded(breedsResponse, items);
        createDescriptionItemIfNeeded(breedsResponse, items);
        createLinkItemIfNeeded(breedsResponse, items);

        return new InternalDetail(breedsResponse.name, imageResponse.uri,
                Collections.unmodifiableList(items));
    }

    private void createDescriptionItemIfNeeded(@NonNull final BreedsResponse response,
                                               @NonNull final List<Item> items) {
        final String description = response.description;
        if (description != null && !description.isEmpty()) {
            items.add(new DescriptionItem(description));
        }
    }

    private void createCountryItemIfNeeded(@NonNull final BreedsResponse response,
                                               @NonNull final List<Item> items) {
        final String countryName = response.countryName;
        if (countryName != null && !countryName.isEmpty()) {
            items.add(new CountryItem(countryName, response.countryCode));
        }
    }

    private void createTemperamentItemIfNeeded(@NonNull final BreedsResponse response,
                                           @NonNull final List<Item> items) {
        final String temperament = response.temperament;
        if (temperament != null && !temperament.isEmpty()) {
            items.add(new TemperamentItem(temperament));
        }
    }

    private void createLinkItemIfNeeded(@NonNull final BreedsResponse response,
                                               @NonNull final List<Item> items) {
        final String wikiUri = response.wikiUri;
        if (wikiUri != null && !wikiUri.isEmpty()) {
            items.add(new LinkItem(wikiUri));
        }
    }

    private boolean isValid(@Nullable final ImageResponse imageResponse) {
        return imageResponse != null && imageResponse.uri != null;
    }

    static final class InternalDetail implements Detail {

        private static final long serialVersionUID = -3333274880693705994L;

        private final String name;
        private final String imageUri;
        private final List<Item> items;

        InternalDetail(final String name, final String imageUri, final List<Item> items) {
            this.name = name;
            this.imageUri = imageUri;
            this.items = items;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getImageUri() {
            return imageUri;
        }

        @Override
        public List<Item> getItems() {
            return items;
        }
    }
}