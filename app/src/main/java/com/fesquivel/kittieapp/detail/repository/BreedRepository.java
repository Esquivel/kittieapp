package com.fesquivel.kittieapp.detail.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.RepositoryCallback;
import com.fesquivel.kittieapp.commons.SchedulerProvider;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.home.repository.BreedApi;

import io.reactivex.disposables.Disposable;

class BreedRepository implements DetailRepository<Detail> {

    private final BreedApi api;
    private final SchedulerProvider schedulerProvider;
    private final DetailMapper mapper;

    @Nullable
    private Disposable disposable;
    @Nullable
    private RepositoryCallback<Detail> callback;

    BreedRepository(final BreedApi api, final SchedulerProvider schedulerProvider,
                    final DetailMapper mapper) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
        this.mapper = mapper;
    }

    @Override
    public void get(@NonNull final String breedId) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        disposable = api.getImage(breedId)
                .map(mapper::mapTo)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(this::onSuccess, this::onFailed);
    }

    @Override
    public void setCallback(@Nullable final RepositoryCallback<Detail> callback) {
        this.callback = callback;
    }

    private void onSuccess(final Detail detail) {
        if (callback != null) {
            callback.onSuccess(detail);
        }
    }

    private void onFailed(final Throwable throwable) {
        if (callback != null) {
            callback.onFailed(throwable);
        }
    }
}