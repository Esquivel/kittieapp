package com.fesquivel.kittieapp.detail.domain;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        DetailType.DESCRIPTION,
        DetailType.COUNTRY,
        DetailType.TEMPERAMENT,
        DetailType.LINK
})
public @interface DetailType {
    int DESCRIPTION = 0;
    int COUNTRY = 1;
    int TEMPERAMENT = 2;
    int LINK = 3;
}