package com.fesquivel.kittieapp.detail.ui;

import com.fesquivel.kittieapp.commons.arch.Mvp;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.domain.Item;

import java.util.List;

public interface DetailView<T extends Mvp.Presenter> extends Mvp.View<T> {

    /**
     * Shows the loading.
     *
     * @param enabled True if show.
     */
    void showLoading(final boolean enabled);

    /**
     * Show title.
     *
     * @param title The title.
     */
    void showTitle(final String title);

    /**
     * Show the image.
     *
     * @param uri The uri.
     */
    void showImage(final String uri);

    /**
     * Show data.
     *
     * @param items The data.
     */
    void showItemData(final List<Item> items);

    /**
     * Starts screen.
     *
     * @param uri The uri.
     */
    void startDeeplinkScreen(final String uri);

    /**
     * Show a error messagge.
     */
    void showErrorMessage();
}
