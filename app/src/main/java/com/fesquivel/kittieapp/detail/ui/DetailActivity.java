package com.fesquivel.kittieapp.detail.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fesquivel.kittieapp.R;
import com.fesquivel.kittieapp.commons.arch.BaseActivity;
import com.fesquivel.kittieapp.detail.domain.Detail;
import com.fesquivel.kittieapp.detail.repository.DetailContainer;
import com.fesquivel.kittieapp.domain.Item;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailActivity extends BaseActivity<DetailState, DetailPresenter>
        implements DetailView<DetailPresenter> {

    private static final String BREED_ID = "breed_id";

    private DetailAdapter detailAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView imageView;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        setToolbar();

        detailAdapter = new DetailAdapter();

        final RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(detailAdapter);

        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(() -> getPresenter().onRefresh());

        imageView = findViewById(R.id.image);
    }

    @Override
    protected DetailPresenter onCreatePresenter() {
        return new DetailPresenter(new DetailContainer().provideRepository());
    }

    @Override
    protected DetailState onCreateState() {
        final Bundle bundle = getIntent().getExtras();
        final String breedId = bundle.getString(BREED_ID);
        return new DetailState(breedId != null ? breedId : "");
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final boolean value = super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return value;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            getPresenter().onRefresh();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().view = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        detailAdapter.setCallback(uri -> getPresenter().onExternalButtonClicked(uri));
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPresenter().view = null;
    }

    private void setToolbar() {
        final Toolbar toolbar = findViewById(R.id.toolbarCollapsible);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    public void showLoading(final boolean enabled) {
        swipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showTitle(final String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showImage(final String uri) {
        Picasso.with(this)
                .load(uri)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    @Override
    public void showItemData(final List<Item> items) {
        detailAdapter.setData(items);
    }

    @Override
    public void startDeeplinkScreen(final String uri) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public void showErrorMessage() {
        Snackbar.make(swipeRefreshLayout, getString(R.string.retry_error_message),
                Snackbar.LENGTH_SHORT).show();
    }
}