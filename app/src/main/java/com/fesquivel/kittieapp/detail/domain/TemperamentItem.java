package com.fesquivel.kittieapp.detail.domain;

public final class TemperamentItem extends TextItem {

    private static final long serialVersionUID = 5677336669169372828L;

    public TemperamentItem(final String text) {
        super(text);
    }

    @Override
    public int getType() {
        return DetailType.TEMPERAMENT;
    }
}