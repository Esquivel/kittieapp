package com.fesquivel.kittieapp.detail.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.kittieapp.commons.RepositoryCallback;

public interface DetailRepository<T> {

    /**
     * Get data.
     *
     * @param breedId The id.
     */
    void get(@NonNull final String breedId);

    /**
     * Set callback.
     *
     * @param callback The callback.
     */
    void setCallback(@Nullable final RepositoryCallback<T> callback);
}
