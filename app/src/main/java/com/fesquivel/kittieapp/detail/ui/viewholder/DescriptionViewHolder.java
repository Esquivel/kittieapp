package com.fesquivel.kittieapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.kittieapp.R;

public class DescriptionViewHolder extends RecyclerView.ViewHolder {

    private final TextView descriptionTextView;

    public DescriptionViewHolder(@NonNull final View itemView) {
        super(itemView);

        descriptionTextView = itemView.findViewById(R.id.descriptionText);
    }

    public TextView getDescriptionTextView() {
        return descriptionTextView;
    }
}