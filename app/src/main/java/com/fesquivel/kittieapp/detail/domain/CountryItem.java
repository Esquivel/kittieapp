package com.fesquivel.kittieapp.detail.domain;

public final class CountryItem extends TextItem {

    private static final long serialVersionUID = 5677336669169372828L;

    private final String code;

    public CountryItem(final String text, final String code) {
        super(text);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public int getType() {
        return DetailType.COUNTRY;
    }
}
