package com.fesquivel.kittieapp.detail.ui;

public interface AdapterCallback {

    /**
     * Called when user click button to an external link.
     *
     * @param uri The uri.
     */
    void onExternalButtonClicked(final String uri);
}
