package com.fesquivel.kittieapp.login.repository.login

data class Response(val id: String?, val name: String?, val message: String?)