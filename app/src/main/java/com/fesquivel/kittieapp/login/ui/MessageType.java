package com.fesquivel.kittieapp.login.ui;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        MessageType.IDLE,
        MessageType.LOADING,
        MessageType.DIALOG
})
public @interface MessageType {
    int IDLE = 0;
    int LOADING = 1;
    int DIALOG = 2;
}