package com.fesquivel.kittieapp.login.domain

interface User {

    /**
     * Returns the id of a [User].
     */
    fun getId(): String

    /**
     * Returns the name of a [User].
     */
    fun getName(): String

}