package com.fesquivel.kittieapp.login.repository.user

import com.fesquivel.kittieapp.login.domain.User

internal class InternalUser(
    private val id: String,
    private val name: String
) : User {

    override fun getId(): String {
        return id
    }

    override fun getName(): String {
        return name
    }
}
