package com.fesquivel.kittieapp.login.ui

class MessageFactory() {

    fun getMessage(message: String, @MessageType type: Int): Message? {
        return when (type) {
            MessageType.LOADING -> LoadingMessage()
            MessageType.DIALOG -> DialogMessage(message)
            else -> IdleMessage()
        }
    }

    private class IdleMessage : Message {
        override fun getText(): String {
            return ""
        }

        override fun getType(): Int {
            return MessageType.IDLE
        }
    }

    private class LoadingMessage : Message {
        override fun getText(): String {
            return ""
        }

        override fun getType(): Int {
            return MessageType.LOADING
        }
    }

    private class DialogMessage(private val text: String) : Message {

        override fun getText(): String {
            return text
        }

        override fun getType(): Int {
            return MessageType.DIALOG
        }
    }
}