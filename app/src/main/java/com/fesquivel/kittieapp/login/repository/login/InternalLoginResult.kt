package com.fesquivel.kittieapp.login.repository.login

import com.fesquivel.kittieapp.login.domain.LoginResult
import com.fesquivel.kittieapp.login.domain.User

internal data class InternalLoginResult(
    private val user: User?,
    private val serverMessage: String
): LoginResult {

    override fun getUser(): User? {
        return user
    }

    override fun getServerMessage(): String? {
        return serverMessage
    }
}
