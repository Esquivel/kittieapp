package com.fesquivel.kittieapp.login.repository.login

import androidx.lifecycle.MutableLiveData
import com.fesquivel.kittieapp.commons.SchedulerProvider
import com.fesquivel.kittieapp.login.domain.LoginResult
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

private const val SIMULATION_REQUEST_TIME: Long = 3000

internal class UserLoginRepository(
    private val schedulerProvider: SchedulerProvider,
    private val loginApi: LoginApi,
    private val loginMapper: LoginMapper
) : LoginRepository {

    private val loginLiveData = MutableLiveData<LoginResult>()
    private var disposable: Disposable? = null

    override fun getLiveData(): MutableLiveData<LoginResult> {
        return loginLiveData
    }

    override fun login(username: String, password: String) {
        disposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }

        disposable = loginApi.get(username, password)
            .map { t-> loginMapper.mapToUser(t) }
            .delaySubscription(SIMULATION_REQUEST_TIME, TimeUnit.MILLISECONDS)
            .observeOn(schedulerProvider.io())
            .subscribeOn(schedulerProvider.ui())
            .subscribe(this::onSuccess, this::onFailed)
    }

    private fun onSuccess(loginResult: LoginResult) {
        loginLiveData.postValue(loginResult)
    }

    private fun onFailed(throwable: Throwable) {
        loginMapper.mapToUser(null).also {
            loginLiveData.postValue(it)
        }
    }
}