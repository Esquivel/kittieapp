package com.fesquivel.kittieapp.login.repository.login;

import com.fesquivel.kittieapp.commons.SchedulerProvider;

import io.reactivex.Observable;

public class LoginContainer {

    private LoginRepository repository;
    private SchedulerProvider schedulerProvider;
    private LoginApi api;
    private LoginMapper loginMapper;

    public LoginRepository provideLoginRepository() {
        if (repository == null) {
            repository = new UserLoginRepository(provideSchedulerProvider(), provideLoginApi(),
                    provideLoginMapper());
        }
        return repository;
    }

    private SchedulerProvider provideSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProvider();
        }
        return schedulerProvider;
    }

    private LoginApi provideLoginApi() {
        if (api == null) {
            api = (username, password) ->
                    Observable.just(new Response("", "", ""));
        }
        return api;
    }

    private LoginMapper provideLoginMapper() {
        if (loginMapper == null) {
            loginMapper = new LoginMapper();
        }
        return loginMapper;
    }
}