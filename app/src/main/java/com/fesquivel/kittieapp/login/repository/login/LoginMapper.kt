package com.fesquivel.kittieapp.login.repository.login

import androidx.annotation.WorkerThread
import com.fesquivel.kittieapp.login.domain.LoginResult
import com.fesquivel.kittieapp.login.repository.user.InternalUser

private const val DEFAULT_NAME = "default_name"
private const val FAKE_SERVER_MESSAGE = "Please, retry operation later"

class LoginMapper {

    /**
     * Map to LoginResult or return null.
     * Do some validations.
     *
     * @param response The Response to map.
     */
    @WorkerThread
    fun mapToUser(response: Response?) : LoginResult {
        if (response == null) return InternalLoginResult(null,
            FAKE_SERVER_MESSAGE
        )

        val validatedName = response.name ?: DEFAULT_NAME

        response.id?.let {
            return InternalLoginResult(
                InternalUser(
                    it,
                    validatedName
                ), "")
        }

        return InternalLoginResult(null,
            FAKE_SERVER_MESSAGE
        )
    }
}