package com.fesquivel.kittieapp.login.repository.user

import androidx.lifecycle.LiveData
import com.fesquivel.kittieapp.login.domain.User

interface UserRepository {

    /**
     * Get information about user.
     *
     * @return A liveData of User. Could be null.
     */
    fun get(): LiveData<User?>

    /**
     * Set the user information. Could be null.
     *
     * @param user The [User] to save.
     */
    fun set(user: User)
}