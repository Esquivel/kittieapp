package com.fesquivel.kittieapp.login.repository.user

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fesquivel.kittieapp.login.domain.User
import com.google.gson.Gson

private const val USER_KEY = "user_key"

internal class LocalUserRepository(
    private val preferences: SharedPreferences,
    private val gson: Gson
) : UserRepository {

    private val userLiveData = MutableLiveData<User?>()

    override fun get(): LiveData<User?> {
        gson.fromJson(preferences.getString(USER_KEY, null),
            InternalUser::class.java).also {
            userLiveData.value = it
        }

        return userLiveData
    }

    override fun set(user: User) {
        preferences.edit().putString(USER_KEY, gson.toJson(user)).apply().also {
            get()
        }
    }
}