package com.fesquivel.kittieapp.login.repository.login

import io.reactivex.Observable

/**
 * This interface simulates the Retrofit interface to do an Api request.
 */
interface LoginApi {

    /**
     * @GET("login/")
     *
     * @param username The username.
     * @param password The password.
     * @return The response wrapped on a Observable.
     */
    fun get(username: String, password: String): Observable<Response>
}