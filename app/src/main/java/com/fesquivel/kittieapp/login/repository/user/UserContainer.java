package com.fesquivel.kittieapp.login.repository.user;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class UserContainer {

    private static final String USER_PREFERENCES = "user_preferences";

    private UserRepository repository;
    private SharedPreferences preferences;
    private Gson gson;

    public UserRepository provideUserRepository(final Context context) {
        if (repository == null) {
            repository = new LocalUserRepository(providePreferences(context), provideGson());
        }
        return repository;
    }

    private Gson provideGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    private SharedPreferences providePreferences(final Context context) {
        if (preferences == null) {
            preferences = context.getSharedPreferences(USER_PREFERENCES, 0);
        }
        return preferences;
    }
}