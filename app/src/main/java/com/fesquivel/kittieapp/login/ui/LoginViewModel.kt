package com.fesquivel.kittieapp.login.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fesquivel.kittieapp.login.domain.LoginResult
import com.fesquivel.kittieapp.login.domain.User
import com.fesquivel.kittieapp.login.repository.login.LoginRepository
import com.fesquivel.kittieapp.login.repository.user.UserRepository

class LoginViewModel(
    private val loginRepository: LoginRepository,
    private val userRepository: UserRepository,
    private val messageFactory: MessageFactory
) : ViewModel() {

    private val loginLiveData: MutableLiveData<LoginResult> = loginRepository.getLiveData()
    private val userLiveData: LiveData<User?> = userRepository.get()
    private val messageLiveData = MutableLiveData<Message>()

    /**
     * Returns the loading data.
     */
    fun getMessage() : LiveData<Message> = messageLiveData

    /**
     * Returns the user data.
     */
    fun getUserData() : LiveData<User?> = userLiveData

    /**
     * Returns the login result data.
     */
    fun getLoginResultData() : LiveData<LoginResult> = loginLiveData

    /**
     * Called when the user click the login button.
     *
     * @param username The username filled by user.
     * @param password The password filled by user.
     */
    fun onLoginClicked(username: String, password: String, invalidMessage: String) {
        if (username.isEmpty() || password.isEmpty()) {
            showInvalidFieldsMessage(invalidMessage)
            return
        }

        doLogin(username, password).also { showLoading(true) }
    }

    /**
     * Called when a update has reached.
     *
     * @param loginResult The login result.
     */
    fun onLoginResultReceived(loginResult: LoginResult) {
        showLoading(false)

        loginResult.getUser()?.let { user ->
            saveUser(user)
            return
        }

        showDialogMessage(loginResult.getServerMessage() ?:"")
    }

    private fun doLogin(username: String, password: String) {
        loginRepository.login(username, password)
    }

    private fun saveUser(user: User) {
        userRepository.set(user)
    }

    private fun showLoading(enabled: Boolean) {
        val type = if (enabled) MessageType.LOADING else MessageType.IDLE
        messageLiveData.value = messageFactory.getMessage("", type)
    }

    private fun showDialogMessage(text: String) {
        messageLiveData.value = messageFactory.getMessage(text, MessageType.DIALOG)
    }

    private fun showInvalidFieldsMessage(invalidMessage: String) {
        messageLiveData.value = messageFactory.getMessage(invalidMessage,
            MessageType.DIALOG)
    }
}