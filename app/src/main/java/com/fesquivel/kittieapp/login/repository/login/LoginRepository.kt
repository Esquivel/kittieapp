package com.fesquivel.kittieapp.login.repository.login

import androidx.lifecycle.MutableLiveData
import com.fesquivel.kittieapp.login.domain.LoginResult

interface LoginRepository {

    /**
     * Returns the liveData to be updated.
     */
    fun getLiveData() : MutableLiveData<LoginResult>

    /**
     * Return a liveData with the [LoginResult] instance.
     */
    fun login(username: String, password: String)
}