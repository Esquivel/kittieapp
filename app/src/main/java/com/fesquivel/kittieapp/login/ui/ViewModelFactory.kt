package com.fesquivel.kittieapp.login.ui

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.fesquivel.kittieapp.login.repository.login.LoginRepository
import com.fesquivel.kittieapp.login.repository.user.UserRepository

class ViewModelFactory(
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null,
    private val loginRepository: LoginRepository,
    private val userRepository: UserRepository,
    private val messageFactory: MessageFactory
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>,
                                         state: SavedStateHandle
    ): T {
        return LoginViewModel(loginRepository, userRepository, messageFactory) as T
    }
}