package com.fesquivel.kittieapp.login.ui

/**
 * Message to show to the User.
 */
interface Message {

    /**
     * The text to show to the User.
     *
     * @return The text.
     */
    fun getText() : String

    /**
     * The type of message.
     *
     * @return The type.
     */
    @MessageType
    fun getType() : Int
}