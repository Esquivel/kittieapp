package com.fesquivel.kittieapp.login.domain

interface LoginResult {

    /**
     * Returns a [User].
     *
     * @return If the login operation was success.
     */
    fun getUser(): User?

    /**
     * Get the server message. Could return null.
     *
     * @return A message, could be null.
     */
    fun getServerMessage(): String?
}