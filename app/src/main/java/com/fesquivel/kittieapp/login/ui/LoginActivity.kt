package com.fesquivel.kittieapp.login.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fesquivel.kittieapp.R
import com.fesquivel.kittieapp.home.ui.HomeActivity
import com.fesquivel.kittieapp.login.repository.login.LoginContainer
import com.fesquivel.kittieapp.login.repository.user.UserContainer

class LoginActivity : AppCompatActivity() {

    private lateinit var loadingView: View
    private lateinit var usernameView: EditText
    private lateinit var passwordView: EditText
    private var alertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        usernameView = findViewById(R.id.username)
        passwordView = findViewById(R.id.password)
        val loginButton = findViewById<View>(R.id.loginButton)
        loadingView = findViewById(R.id.loading)

        val viewModel = ViewModelProvider(this, ViewModelFactory(this,
            intent.extras, LoginContainer().provideLoginRepository(),
            UserContainer().provideUserRepository(this), MessageFactory()
        )).get(LoginViewModel::class.java)

        viewModel.getMessage().observe(this, Observer {
            when (it.getType()) {
                MessageType.IDLE -> {
                    showLoading(false)
                    showMessageDialog("")
                }
                MessageType.LOADING -> showLoading(true)
                else -> showMessageDialog(it.getText())
            }
        })

        viewModel.getLoginResultData().observe(this, Observer {
            viewModel.onLoginResultReceived(it)
        })

        viewModel.getUserData().observe(this, Observer {
            if (it != null) startListScreen()
        })

        val invalidMessage = getString(R.string.login_message_dialog)

        loginButton.setOnClickListener {
            viewModel.onLoginClicked(username = usernameView.text.toString(),
                password = passwordView.text.toString(), invalidMessage = invalidMessage)
        }
    }

    private fun showLoading(enabled: Boolean) {
        loadingView.visibility = if (enabled) View.VISIBLE else View.GONE
    }

    private fun showMessageDialog(text: String) {
        if (text.isEmpty()) {
            alertDialog?.dismiss()
        } else {
            alertDialog = AlertDialog.Builder(this)
                .setTitle(R.string.login_title_dialog)
                .setMessage(text)
                .show()
        }
    }

    private fun startListScreen() {
        finish()
        startActivity(Intent(this, HomeActivity::class.java))
    }
}