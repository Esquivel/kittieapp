package com.fesquivel.kittieapp.domain;

import com.fesquivel.kittieapp.home.repository.BreedsResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ImageResponse {

    @SerializedName("breeds")
    public final List<BreedsResponse> responseList;
    @SerializedName("url")
    public final String uri;

    public ImageResponse(final List<BreedsResponse> responseList, final String uri) {
        this.responseList = responseList;
        this.uri = uri;
    }
}
