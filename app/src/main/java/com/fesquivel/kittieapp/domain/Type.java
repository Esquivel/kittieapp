package com.fesquivel.kittieapp.domain;

public interface Type {

    /**
     * Returns the type.
     *
     * @return The type.
     */
    int get();
}
