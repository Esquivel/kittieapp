package com.fesquivel.kittieapp.domain;

import java.io.Serializable;

public interface Item extends Serializable {

    /**
     * Get the {@link Item} type.
     *
     * @return The type.
     */
    int getType();
}