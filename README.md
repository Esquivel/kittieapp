# Kittie APP

Welcome to this sample project. You will find an very simple application with three screens:

- [LoginActivity](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/login/ui/LoginActivity.kt):
    A simple Login screen, validates if exists a stored data, if exists redirects to the HomeActivity.
- [HomeActivity](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/home/ui/HomeActivity.kt):
    Here find a collection of [Items](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/domain/Item.java). This items can handle events like Click.
    This Activity has a filter by country section. 
    Also will find a swipe to refresh.
    Perhaps the most important aspect in this layer is the treatment of the Android lifecycle, reconstruction and the related Presenter State.

- [DetailActivity](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/detail/ui/DetailActivity.java):
    Here find the detail of the [Cat](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/home/domain/Cat.java) selected. Here, again, can refresh your data.
    The detail screen has a heterogeneous layout inside RecyclerView. This architecture is flexible, simple and retrocompatible. Those types the View not know how draw will be filtered on the Model layer [Repository](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/detail/repository/DetailRepository.java) by a [Mapper](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/detail/repository/DetailMapper.java). 

Will find the use of abstractions over implementations.
The best example is the [Item](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/domain/Item.java) interface.
Item only exposes a type, and this type is used to determine the show behaviour. Also will find the [Filter](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/home/domain/Filter.java) and [Cat](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/main/java/com/fesquivel/kittieapp/home/domain/Cat.java) interfaces.

Other important aspect about the design is that all objects are inmutable. The mapper layer is responsible to instantiation and validation of that objects.

## UX

This app is inspired by the [Material Design Guidelines](https://material.io/design).
It was designed supporting [accessibility](https://developer.android.com/guide/topics/ui/accessibility) best practices  

## Architecture
Here will find examples to how implement two different Architecture patterns:

- [Model View Presenter](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) 
- [Model View ViewModel](https://developer.android.com/jetpack/guide#recommended-app-arch) This is a special implementation developed by Google included on [JetPack Libraries](https://developer.android.com/jetpack)

## Languages
This app was developed mixing Java and Kotlin.

## Unit test
You will find examples of how test two extremely different layers: [Mapper](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/test/java/com/fesquivel/kittieapp/detail/repository/DetailMapperTest.java) and [Presenter](https://gitlab.com/Esquivel/kittieapp/-/blob/master/app/src/test/java/com/fesquivel/kittieapp/detail/ui/DetailPresenterTest.java)